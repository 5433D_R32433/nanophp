#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>

typedef enum token_kind_t
{
		
    TOKEN_EOF,
	
	
    TOKEN_NUMBER,
    TOKEN_ADD,
    TOKEN_SUBTRACT,
    TOKEN_MULTIPLY,
    TOKEN_DIVIDE,
    TOKEN_MODULUS,
	TOKEN_EXPONENTIATION,
	
	TOKEN_INCREMENT,
	TOKEN_DECREMENT,
	
	
	TOKEN_VARIABLE,
	
    TOKEN_LPAREN,
    TOKEN_RPAREN,
	TOKEN_LBRACKET,
	TOKEN_RBRACKET,
	TOKEN_LCURLY_BRACE,
	TOKEN_RCURLY_BRACE,
	
		
	TOKEN_SEMICOLON,
	TOKEN_COLON,
	TOKEN_COMMA,
	
	TOKEN_NEW,
	TOKEN_PUBLIC,
	
	TOKEN_CLASS,
	TOKEN_FUNCTION,
	TOKEN_ARROW,
	TOKEN_RETURN,
	
	
	TOKEN_ASSIGN,
	TOKEN_ADD_ASSIGN,
	TOKEN_SUBTRACT_ASSIGN,
	TOKEN_MULTIPLY_ASSIGN,
	TOKEN_DIVIDE_ASSIGN,
	TOKEN_MODULUS_ASSIGN,
	TOKEN_EQUAL,
	TOKEN_IDENTICAL,
	TOKEN_NOT_EQUAL,
	TOKEN_NOT_IDENTICAL,
	TOKEN_GREATER,
	TOKEN_LESS,
	TOKEN_GREATER_EQUAL,
	TOKEN_LESS_EQUAL,
	TOKEN_SPACESHIP,
	
	TOKEN_STATEMENT,
	
	
	TOKEN_ECHO,
	TOKEN_PRINT,
	
	TOKEN_FOR,
	TOKEN_FOREACH,
	TOKEN_WHILE,
	TOKEN_DO_WHILE,
	TOKEN_BREAK,
	TOKEN_CONTINUE,
	
	
	TOKEN_STRING,
	
	TOKEN_IF,
	TOKEN_ELSEIF,
	TOKEN_ELSE,
	TOKEN_SWITCH,
	TOKEN_CASE,
	TOKEN_DEFAULT,
	
	TOKEN_OR,
	TOKEN_AND,
	TOKEN_DOT,
	
	TOKEN_STATIC,
	TOKEN_GLOBAL,
	
	TOKEN_IDENTIFIER,
} token_kind_t;

char *token_kind_string [ ] =
{
	"TOKEN_EOF",
		
	"TOKEN_NUMBER",
    "TOKEN_ADD",
    "TOKEN_SUBTRACT",
    "TOKEN_MULTIPLY",
    "TOKEN_DIVIDE",
    "TOKEN_MODULUS",
	"TOKEN_EXPONENTIATION",
	"TOKEN_INCREMENT",
	"TOKEN_DECREMENT",
	"TOKEN_VARIABLE",
    "TOKEN_LPAREN",
    "TOKEN_RPAREN",
	"TOKEN_LBRACKET",
	"TOKEN_RBRACKET",
	"TOKEN_LCURLY_BRACE",
	"TOKEN_RCURLY_BRACE",
	"TOKEN_SEMICOLON",
	"TOKEN_COLON",
	"TOKEN_COMMA",
	"TOKEN_NEW",
	"TOKEN_PUBLIC",
	"TOKEN_CLASS",
	"TOKEN_FUNCTION",
	"TOKEN_ARROW",
	"TOKEN_RETURN",
	"TOKEN_ASSIGN",
	"TOKEN_ADD_ASSIGN",
	"TOKEN_SUBTRACT_ASSIGN",
	"TOKEN_MULTIPLY_ASSIGN",
	"TOKEN_DIVIDE_ASSIGN",
	"TOKEN_MODULUS_ASSIGN",
	"TOKEN_EQUAL",
	"TOKEN_IDENTICAL",
	"TOKEN_NOT_EQUAL",
	"TOKEN_NOT_IDENTICAL",
	"TOKEN_GREATER",
	"TOKEN_LESS",
	"TOKEN_GREATER_EQUAL",
	"TOKEN_LESS_EQUAL",
	"TOKEN_SPACESHIP",
	"TOKEN_STATEMENT",
	
	"TOKEN_ECHO",
	"TOKEN_PRINT",
	"TOKEN_FOR",
	"TOKEN_FOREACH",
	"TOKEN_WHILE",
	"TOKEN_DO_WHILE",
	"TOKEN_BREAK",
	"TOKEN_CONTINUE",
	"TOKEN_STRING",
	"TOKEN_IF",
	"TOKEN_ELSEIF",
	"TOKEN_ELSE",
	"TOKEN_SWITCH",
	"TOKEN_CASE",
	"TOKEN_DEFAULT",
	"TOKEN_OR",
	"TOKEN_AND",
	"TOKEN_DOT",
	"TOKEN_STATIC",
	"TOKEN_GLOBAL",
	"TOKEN_IDENTIFIER",
	
};


typedef struct token_t
{
    token_kind_t type;
    int value; 
	char *lexem;
} token_t;

typedef struct ast_node_t 
{
    token_kind_t type;
    int value;
	char *variable_name;
    struct ast_node_t* left;
    struct ast_node_t* right;
} ast_node_t;


char* input           = 0;
int current_position  = 0;
token_t current_token = { 0 };


typedef struct symbol_t 
{
    char* name;
    ast_node_t *expression;
} symbol_t;

symbol_t symbol_table [ 0x1000 ] = { 0 };


char next ( ) 
{
    return input [ current_position++ ];
}

char prev ( )
{
	return input [ current_position-- ];
}

char current ( )
{
	return input [ current_position ];
}

char look_back  ( )
{
	return input [ current_position ];
}


token_t scan_keyword ( char c )
{
	char keyword [ 64 * 1024 ] = { 0 };
    int i = 0;
    while ( isalnum ( c ) ) 
	{
        keyword [ i++ ] = c;
        c = next ( );
    }   
	keyword [ i ] = '\0';

    if ( !stricmp ( keyword, "echo" ) )
	{
        return ( token_t ) { TOKEN_ECHO, 0, strdup ( keyword ) };
    }
	else if ( !stricmp ( keyword, "print") )
	{
		return ( token_t ) { TOKEN_PRINT, 0, strdup ( keyword ) };
	}
	else if ( !stricmp ( keyword, "if") )
	{
		return ( token_t ) { TOKEN_IF, 0, strdup ( keyword ) };
	}
	else if ( !stricmp ( keyword, "else") )
	{
		return ( token_t ) { TOKEN_ELSE, 0, strdup ( keyword ) };
	}
	else if ( !stricmp ( keyword, "elseif") )
	{
		return ( token_t ) { TOKEN_ELSEIF, 0, strdup ( keyword ) };
	}
	else if ( !stricmp ( keyword, "switch") )
	{
		return ( token_t ) { TOKEN_SWITCH, 0, strdup ( keyword ) };
	}
	else if ( !stricmp ( keyword, "case") )
	{
		return ( token_t ) { TOKEN_CASE, 0, strdup ( keyword ) };
	}
	else if ( !stricmp ( keyword, "default") )
	{
		return ( token_t ) { TOKEN_DEFAULT, 0, strdup ( keyword ) };
	}
	else if ( !stricmp ( keyword, "or") )
	{
		return ( token_t ) { TOKEN_OR, 0, strdup ( keyword ) };
	}
	else if ( !stricmp ( keyword, "and") )
	{
		return ( token_t ) { TOKEN_AND, 0, strdup ( keyword ) };
	}
	else if ( !stricmp ( keyword, "do") )
	{
		return ( token_t ) { TOKEN_DO_WHILE, 0, strdup ( keyword ) };
	}
	else if ( !stricmp ( keyword, "while") )
	{
		return ( token_t ) { TOKEN_WHILE, 0, strdup ( keyword ) };
	}
	else if ( !stricmp ( keyword, "for") )
	{
		return ( token_t ) { TOKEN_FOR, 0, strdup ( keyword ) };
	}
	else if ( !stricmp ( keyword, "foreach") )
	{
		return ( token_t ) { TOKEN_FOREACH, 0, strdup ( keyword ) };
	}
	else if ( !stricmp ( keyword, "function") )
	{
		return ( token_t ) { TOKEN_FOREACH, 0, strdup ( keyword ) };
	}
	else
	{
		fprintf ( stderr, "%s is not a key word!\n", keyword  );
		exit    ( EXIT_FAILURE );
	}
}

token_t scan_variable ( char c )
{
	char variable_name [ 64 * 1024 ] = { 0 };
    int i = 0;
	c = next ( );
	if ( isalpha ( c ) || c == '_' )
	{	
		while ( isalnum ( c ) || c == '_' && i < ( 64 * 1024 ) ) 
		{
			variable_name [ i++ ] = c;
			c = next ( );
		}
		return ( token_t ) { TOKEN_VARIABLE, 0, strdup ( variable_name ) };
	}
	else
	{
		fprintf ( stderr, "Variable Name Cannot Be Started With: \n", c );
		exit    ( EXIT_FAILURE );
		
	}
}

token_t scan_identifier ( char c )
{
	int i = 0;
	char identifier [ 256 ] = { 0 };	
	while ( !isspace ( c ) )
	{
		identifier [ i++ ] = c;
		c = next ( );
	}
	identifier [ i ] = '0';
	
	return ( token_t ) { TOKEN_IDENTIFIER, 0, strdup ( identifier ) };
}

token_t scan_string_literal ( char c )
{
	char string_literal [ 64 * 1024 ] = { 0 };
    int i = 0;
	
	if ( c == '"' )
	{
		c = next ( );
		while ( c != '"' && c != '\0' && i < ( 64 * 1024 ) )
		{
			string_literal [ i++ ] = c;
			c = next ( );
		}
		
		if ( c == '"' )
		{
			return ( token_t ) { TOKEN_STRING, 0, strdup ( string_literal ) };
		}
		
	}
	if ( c == '\'' )
	{
		c = next ( );
		while ( c != '\'' && c != '\0' && i < ( 64 * 1024 ) )
		{
			string_literal [ i++ ] = c;
			c = next ( );
		}
		
		if ( c == '\'' )
		{
			return ( token_t ) { TOKEN_STRING, 0, strdup ( string_literal ) };
		}
	}
	fprintf ( stderr, "Unterminated string literal\n" );
	exit    ( EXIT_FAILURE);	
}

token_t scan_number ( char c )
{
	int64_t value = 0;
    while ( isdigit ( c ) ) 
	{
		value = value * 10 + ( c - '0' );
		c = next ( );  
    }
    return ( token_t ) { TOKEN_NUMBER, value, NULL };
}



token_t next_token ( ) 
{
	char c = next ( );

    while ( isspace ( c ) ) 
	{
        c = next ( );
    }

    if ( c == '\0' ) 
	{
        return ( token_t ) { TOKEN_EOF, 0, 0 };
    }
	
	if ( c == '+' )
	{
		if ( current ( ) == '+' )
		{
			return ( token_t ) { TOKEN_INCREMENT, 0, "++" };
		}
		else if ( current ( ) == '=' )
		{
			return ( token_t ) { TOKEN_ADD_ASSIGN, 0, "+=" };
		}
        return ( token_t ) { TOKEN_ADD, 0, "+" };
    }
	
    if ( c == '-' ) 
	{
		if ( current ( ) == '-' )
		{
			return ( token_t ) { TOKEN_DECREMENT, 0, "--" };
		}
		else if ( current ( ) == '=' )
		{
			return ( token_t ) { TOKEN_SUBTRACT_ASSIGN, 0, "-=" };
		}
        return ( token_t ) { TOKEN_SUBTRACT, 0, "-" };
    }
	
    if ( c == '*' ) 
	{
		if  ( current ( ) == '*' )
		{
			return ( token_t ) { TOKEN_EXPONENTIATION, 0, "**" };
		}
		return ( token_t ) { TOKEN_MULTIPLY, 0, "*" };
    }
	
    if ( c == '/' ) 
	{
		if ( current ( ) == '=' )
		{
			return ( token_t ) { TOKEN_DIVIDE_ASSIGN, 0, "/=" };
		}
        return ( token_t ) { TOKEN_DIVIDE, 0, "/" };
    }

    if ( c == '=' )
    {
		if ( current ( ) == '=' )
		{
			if ( current ( ) == '=' )
			{
				return ( token_t ) { TOKEN_IDENTICAL, 0, "===" };
			}
			return ( token_t ) { TOKEN_EQUAL, 0, "==" };
		}				
        return ( token_t ) { TOKEN_ASSIGN, 0, "=" };
    }
	
	
	if ( c == '>' )
	{
		if ( current ( ) == '=' )
		{
			return ( token_t ) { TOKEN_GREATER_EQUAL, 0, ">=" };
		}				
        return ( token_t ) { TOKEN_GREATER, 0, ">" };
		
	}
	
	if ( c == '<' )
	{
		if ( current ( ) == '=' )
		{
			if ( current ( ) == '>' )
			{
				return ( token_t ) { TOKEN_SPACESHIP, 0, "<=>" };
			}
			return ( token_t ) { TOKEN_LESS_EQUAL, 0, "<=" };
		}				
        return ( token_t ) { TOKEN_LESS, 0, "<" };		
	}
	
	if ( c == '!' )
	{
		if ( current ( ) == '=' )
		{
			if ( current ( ) == '=' )
			{
				return ( token_t ) { TOKEN_NOT_IDENTICAL, 0, "!==" };
			}
			return ( token_t ) { TOKEN_NOT_EQUAL, 0, "!=" };
		}
	}
	
	if ( c == '%' ) 
	{
		if ( current ( ) == '=' )
		{
			return ( token_t ) { TOKEN_MODULUS_ASSIGN, 0, "%=" };
		}
        return ( token_t ) { TOKEN_MODULUS, 0, "%" };
    }
	if ( c == '&' )
	{
		return ( token_t ) { TOKEN_AND, 0, "&" };
	}
	
	if ( c== '|' )
	{
		return ( token_t ) { TOKEN_OR, 0, "|" };
	}
	
    if ( c == '(' ) 
	{
        return ( token_t ) { TOKEN_LPAREN, 0, "(" };
    }
	
    if ( c == ')' ) 
	{
        return ( token_t ) { TOKEN_RPAREN, 0, ")" };
    }
	
	if ( c == ';' ) 
	{
        return ( token_t ) { TOKEN_SEMICOLON, 0, ";" };
	}
	
	if ( c == '.' ) 
	{
        return ( token_t ) { TOKEN_DOT, 0, "." };
	}
	
	if ( c == ',' )
	{
		return ( token_t ) { TOKEN_COMMA, 0, "," };
	}
	
	if ( c == '{' )
	{
		return ( token_t ) { TOKEN_LCURLY_BRACE, 0, "{" };
	}
	
	if ( c == '}' )
	{
		return ( token_t ) { TOKEN_RCURLY_BRACE, 0, "}" };
	}
	
	if ( c == '[' )
	{
		return ( token_t ) { TOKEN_LBRACKET, 0, "[" };
	}
	
	if ( c == ']' )
	{
		return ( token_t ) { TOKEN_RBRACKET, 0, "]" };
	}
	
	if ( c == '$' ) 
	{
		return scan_variable ( c );
    }
	
	if ( isalpha ( c ) ) 
	{
        return scan_keyword ( c );		
	}
	
    if ( isdigit ( c ) ) 
	{
		return scan_number ( c );
    }
	
	if ( c == '_' || isalpha ( c ) )
	{
		return scan_identifier ( c );
	}
	
	if ( c == '"' || c == '\'' )
	{
		return scan_string_literal ( c );
    }
	
	exit ( EXIT_FAILURE );
}

ast_node_t* parse_expression();
ast_node_t* parse_terminal();
ast_node_t* parseFactor();


ast_node_t* make_ast_node ( token_kind_t type, int value, char* variable_name, ast_node_t* left, ast_node_t* right ) 
{
    ast_node_t* node = (ast_node_t*)malloc(sizeof(ast_node_t));
    if (node == NULL) {
        fprintf(stderr, "Memory allocation error.\n");
        exit(EXIT_FAILURE);
    }
    node->type = type;
    node->value = value;
	node->variable_name = variable_name;
    node->left = left;
    node->right = right;
    return node;
}


ast_node_t* parse_factor ( ) 
{
    token_t token = current_token;
    
	if ( token.type == TOKEN_NUMBER ) 
	{
        current_token = next_token ( );
        return make_ast_node ( TOKEN_NUMBER, token.value, NULL, NULL, NULL );
    } 
	
	else if ( token.type == TOKEN_VARIABLE ) 
	{
        current_token = next_token ( );
        return make_ast_node ( TOKEN_VARIABLE, 0, strdup ( token.lexem ), NULL, NULL );
    }
	
	else if ( token.type == TOKEN_LPAREN ) 
	{
        current_token = next_token ( );
        ast_node_t* result = parse_expression ( );
        if ( current_token.type != TOKEN_RPAREN ) 
		{
            exit ( EXIT_FAILURE ); 
        }
        current_token = next_token ( );
        return result;
    } 	
	
	else 
	{
        exit ( EXIT_FAILURE ); 
    }
}


ast_node_t* parse_terminal ( ) 
{
    ast_node_t* left = parse_factor ( );
    while ( current_token.type == TOKEN_MULTIPLY || current_token.type == TOKEN_DIVIDE || current_token.type == TOKEN_MODULUS ) 
	{
        token_t op        = current_token;
        current_token     = next_token();
        ast_node_t* right = parse_factor();
        left              = make_ast_node ( op.type, 0, 0, left, right );
    }
    return left;
}


ast_node_t* parse_expression ( ) 
{
    ast_node_t* left = parse_terminal ( );
    while ( current_token.type == TOKEN_ADD || current_token.type == TOKEN_SUBTRACT ) 
	{
        token_t op         = current_token;
        current_token      = next_token ( );
        ast_node_t* right  = parse_terminal ( );
        left               = make_ast_node  ( op.type, 0, NULL, left, right );
    }
    return left;
}


void print_ast ( ast_node_t* node ) 
{
    if ( node ) 
	{
        print_ast ( node->left );
        if ( node->type == TOKEN_NUMBER ) 
		{
            printf ( "%d ", node->value );
        }            
		else         
		{            
            printf ( "%c ", node->type );
        }
		print_ast ( node->right );
    }
}


int find_variable_value ( char *name ) 
{
    for ( int i = 0; i < 0x1000; i++ ) 
	{
		if ( symbol_table [ i ].name != NULL && strcmp ( symbol_table [ i ].name, name ) == 0 )
		{
			return evaluate_ast ( symbol_table [ i ].expression );
        }
    }
    fprintf ( stderr, "Variable '%s' not found\n", name );
    exit    ( EXIT_FAILURE );
}

void add_variable_expression ( char* name, ast_node_t* expression )
{
	for (int i = 0; i < 0x1000; i++) 
	{
		if ( symbol_table [ i ].name == NULL )
		{
			symbol_table [ i ].name = strdup ( name );
			symbol_table [ i ].expression = expression;
            return;
        }
    }
	fprintf ( stderr, "Symbol table is full\n" );
	exit ( EXIT_FAILURE );
}


int evaluate_ast ( ast_node_t* node ) 
{
    if (node == NULL) 
	{
        return 0;
    } 
	if ( node->type == TOKEN_NUMBER ) 
	{
        return node->value;
    }
	else if ( node->type == TOKEN_VARIABLE ) 
	{
        for ( int i = 0; i < 0x1000; i++ ) 
		{
            if ( symbol_table [ i ].name != NULL && strcmp ( symbol_table [ i ].name, node->variable_name ) == 0 ) 
			{
                return evaluate_ast ( symbol_table [ i ].expression );
            }
        }
        fprintf ( stderr, "Variable '%s' not found\n", node->variable_name );
        exit    ( EXIT_FAILURE );
    }
	else 
	{
        int left  = evaluate_ast ( node->left  );
        int right = evaluate_ast ( node->right );
        switch (node->type) 
		{
            case TOKEN_ADD:
			{
				return left + right;
			}
            
			case TOKEN_SUBTRACT:
			{
				return left - right;
			}
			
            case TOKEN_MULTIPLY:
			{
                return left * right;
			}
			
            case TOKEN_DIVIDE:
			{
                return left / right;
			}
			
            case TOKEN_MODULUS:
			{
                return left % right;
			}
			
            default:
			{
				return 0;
			}
        }
    }
}



bool check_semicolon ( ) 
{
    return ( current_token.type == TOKEN_SEMICOLON );
}


void parse_echo ( ) 
{
    current_token = next_token ( );

    int result = 0; 

    while ( current_token.type == TOKEN_VARIABLE || current_token.type == TOKEN_STRING ) 
	{
        if ( current_token.type == TOKEN_VARIABLE ) 
		{
            char *variable_name = strdup ( current_token.lexem );
            int variable_value  = find_variable_value ( variable_name );
            result             += variable_value;
        } 
		else if ( current_token.type == TOKEN_STRING ) 
		{
            printf ( "%s\n", current_token.lexem );
        }

        current_token = next_token ( );
    }

    if ( check_semicolon ( ) ) 
	{
        printf ( "%d\n", result ); 
        current_token = next_token ( );
    } 
	else
	{
        fprintf ( stderr, "Missing semicolon after 'echo'\n" );
        exit    ( EXIT_FAILURE );
    }
}

void parse_variable ( )
{
	char *variable_name = strdup ( current_token.lexem );
    current_token = next_token ( );
    if ( current_token.type == TOKEN_ASSIGN ) 
	{
        current_token = next_token ( );
        ast_node_t *expression = parse_expression ( );

        if ( current_token.type == TOKEN_SEMICOLON ) 
		{
            current_token = next_token ( );
            add_variable_expression ( variable_name, expression );
            printf ( "%s = %d\n", variable_name, find_variable_value ( variable_name ) );
        } 
		else 
		{
            fprintf ( stderr, "Missing Semicolon\n" );
            exit    ( EXIT_FAILURE );
        }
    } 
	else 
	{
        fprintf ( stderr, "Expected '=' after variable name\n" );
        exit    ( EXIT_FAILURE );
    }
}


void parse_statement ( ) 
{
    if ( current_token.type == TOKEN_VARIABLE ) 
	{
		parse_variable ( );
    } 
	else if ( current_token.type == TOKEN_ECHO ) 
	{
		parse_echo ( );
	} 
	else 
	{
		fprintf ( stderr, "Expected variable name or statement\n" );
		exit    ( EXIT_FAILURE );
	}
}

void parse_else ( )
{
}



void parse_if ( ) 
{
}


//  
/* 
 *	<selection-statement> ::= if ( <expression> ) <statement>
 *                          | if ( <expression> ) <statement> else  <statement>
 *                          | switch ( <expression> ) <statement>
 */

void parse_elseif ( ) 
{
   
}

void parse_function ( void ) 
{
	
	if ( current_token.type == TOKEN_FUNCTION )
	{
		current_token = next_token ( );
		if ( current_token.type == TOKEN_IDENTIFIER )
		{
			
		}
	}
}

void print_token ( token_t t )
{
	printf ( "(%s, %s, %d) ", token_kind_string [ t.type ], t.lexem, t.value );
}

int main() {
    input = "\"fucking\" 'Hamburger' $$";
	// input = "print echo if else for ";
	

	while ( 1 )
	{
		token_t t = next_token ( );
		print_token ( t );
		
		if ( t.type == TOKEN_EOF )
		{
			break;
		}
	}

  

    return EXIT_SUCCESS;
}





#if 0
  while ( current_token.type != TOKEN_EOF ) 
	{
        if ( current_token.type == TOKEN_VARIABLE || current_token.type == TOKEN_STATEMENT || current_token.type == TOKEN_ECHO || current_token.type == TOKEN_STRING ) 
		{
            if ( current_token.type == TOKEN_ECHO ) 
			{
                parse_echo ( );
            } 
			else if ( current_token.type == TOKEN_STRING ) 
			{
                printf ( "%s", current_token.lexem );
                current_token = next_token ( );
            } 
			else
			{
                result = evaluate_ast ( parse_expression ( ) );
                printf ( "Result: %d\n", result ); 
            }
        } 
		else if ( current_token.type == TOKEN_NUMBER )
		{
                result = evaluate_ast ( parse_expression ( ) );
                printf ( "%d\n", result ); 
        }	
		else 
		{
            fprintf ( stderr, "Expected variable name, statement, 'echo', or string\n" );
            exit ( EXIT_FAILURE );
        }
    }
#endif

